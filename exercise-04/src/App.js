import { useState } from 'react';
import './styling.css';
import NewToDoItem from './NewToDoItem.js'

// A to-do list to use for testing purposes
const initialTodos = [
  { description: 'Finish lecture', isComplete: true },
  { description: 'Do homework', isComplete: false },
  { description: 'Sleep', isComplete: true }
];

function ToDoList({items, onTodoStatusChanged, onRemoveItem}) {
  if (items === undefined || items.length === 0) {
      return (
          <p>There are no to-do items!</p>
      );
  }

  const buttonStyle = {
    float: 'right'
  }

  return items.map((item, index) => 
      
        <div key={index} className={item.isComplete ? "coloured" : "notColoured"}>
          <input type="checkbox" onChange={() => onTodoStatusChanged(index, item.isComplete)} value={item.description} checked={item.isComplete}></input>
          <label>{item.description}</label>
          {item.isComplete && <label> (Done!)</label>}
          <button style={buttonStyle} onClick={() => {onRemoveItem(index)}}>Remove</button>
        </div>
      
    )
}

function App() {
  const [todos, changeTodos] = useState(initialTodos);
  return (
    <div>
      <div>
        <h1>My todos</h1>
        <ToDoList items={todos} 
          onTodoStatusChanged={
            (index, status) => {
              todos[index].isComplete = !todos[index].isComplete
              const changedTodos = Object.create(todos);
              changeTodos(changedTodos)
              console.log(index, changedTodos[index].isComplete)
            }
          }
          onRemoveItem={
            (index) => {
              const changedTodos = [...todos]
              changedTodos.splice(index, 1)
              changeTodos(changedTodos)
            }
          }
        />
      </div>
      <div>
        <h1>Add item</h1>
        <NewToDoItem onClickAction={
          (descrip) => {
            // console.log(descrip)
            changeTodos([...todos, {description: descrip, isComplete: false}])
          }
        }></NewToDoItem>
      </div>

    </div>
  );
}

export default App;