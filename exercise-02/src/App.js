
// A to-do list to use for testing purposes
const todos = [
  'Finish lecture',
  'Do homework',
  'Sleep'
];

// To test functionality with a nonexistant or empty todo list, comment out the list
// above, and uncomment one of the following options:
// const todos = null;
// const todos = undefined;
// const todos = [];

function App() {
  return (
    <div>
      <h1>My todos</h1>
      <ToDoList items={todos} />
    </div>
  );
}

function ToDoList({items}) {
  if (items == null || items.length == 0) {
      return (
          <p>There are no to-do items!</p>
      );
  }
  return (
          <ul>
              {items.map((item, index) => <li key={index}>{item}</li>)}
          </ul>
  );
}

export default App;