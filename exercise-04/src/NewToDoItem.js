import { useState } from 'react';

export default function NewToDoItem({onClickAction}) {
    const [inputValue, changeInput] = useState('')
    function handleChange(e) {
        // console.log(e.target.value)
        changeInput(e.target.value)
    }
    const buttonStyle = {
        float: 'right',
        'margin-left': '5px'
    }
    const divStyle = {
        display: 'flex'
    }
    const inputStyle = {
        flex: 1,
        'margin-left': '5px'
    }
    return <div style={divStyle}>
        <label>Description:</label>
        <input style={inputStyle} type="text" value={inputValue} onChange={handleChange}></input>
        <button style={buttonStyle} onClick={() => {onClickAction(inputValue)}}>Add</button>
    </div>
}